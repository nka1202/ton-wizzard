#! /usr/bin/python3

"""
TODO
"""

import validator
import json
import sys
import os
from time import sleep
# from check_config import red_print, green_print


def red_print(string):
    """
    Принимает строку и выводит ее красным цветом.
    :param string: любая строка)
    """
    line = "\033[1;31;40m{}\033[0m".format(string)
    print(line)


def green_print(string):
    """
    Принимает строку и выводит ее зелёным цветом.
    :param string: любая строка)
    """
    line = "\033[1;32;40m{}\033[0m".format(string)
    print(line)


def get_bid(__info):
    """
    Принимает словарь сгеннерированный info_req и получает у пользоаптеля значение ставки в целых
    грамах. Проводит корректность значения.
    :param __info: словарь сгеннерированный info_req.
    :return: строка с значеним ставки в грамах, если ставка корректна иначе False.
    """

    balance = validator.nano_to_gram(__info["balance"])
    balance = int(balance.split('.')[0])

    min_stake = validator.nano_to_gram(__info["min_bid"])
    min_stake = int(min_stake.split('.')[0])

    if (balance + 2) < min_stake:
        err_line = "You cannot bid. You have {}, but the minimum bid is {}!\n".format(balance, min_stake)
        red_print(err_line)
        return False

    sys.stdin = open("/dev/tty")
    bid = None
    while bid not in range(min_stake, balance):
        bid = int(input("Set the size of the bid from {} to {} gram:  ".format(min_stake, balance - 2)))

    bid_chk = input("If you are sure that you want to set a validator bid in {} grams enter 'yes': ".format(bid))
    if bid_chk == 'yes':
        green_print("Ok!\n")
        return str(bid)
    else:
        red_print("Bid not set!\n")
        return False


def node_list():
    """
    Выгружает список нод из servers.json
    :return: словарь со списком нод формата:
    {"имя ноды": {"ip" : "ip адрес",
    "port_srv" : "контрольный порт ноды",
    "port_con" : "порт lite-server",
    "pub" : "публичный_ключ.pub",
    "lite_pub" : "публичный_ключ_lite_server.pub",
    "client" : "закрытый ключ клиента",
    "wal_addr" : "адрес кошелька в BASE64",
    "wal_name" : "имя кошелька",
    "net_name" : "имя сети",
    "comment" : "комментанрий"} }
    """
    with open("servers.json", "r") as file:
        srvs = json.load(file)

    return srvs


def node_choice():
    """
    Позволяет выбрать ноду.
    :return: строка с именем выбранной ноды и словарь с ее свойствами
    формата:
    {"ip" : "ip адрес",
    "port_srv" : "контрольный порт ноды",
    "port_con" : "порт lite-server",
    "pub" : "публичный_ключ.pub",
    "lite_pub" : "публичный_ключ_lite_server.pub",
    "wal_addr" : "адрес кошелька в BASE64",
    "wal_name" : "имя кошелька",
    "net_name" : "имя сети",
    "comment" : "комментанрий"}
    """

    srv_list = node_list()
    s_name = None
    sys.stdin = open("/dev/tty")
    print("\nNode list:")
    for i in srv_list:
        print("\t{} @ {}  => {}".format(i, srv_list[i]["net_name"], srv_list[i]["comment"]))
    else:
        print("\n")
        while s_name not in srv_list.keys():
            s_name = input("Enter node name: ")

    return s_name, srv_list[s_name]


def adnl_loger(__node_name, keys, elect_id):
    """
    Запись ключей в фаил.
    :param __node_name: имя выбранной ноды.
    :param keys: словарь с ключами сгенерированный validator.validator().
    :param elect_id: идентификатор выборов.
    :return: None
    """
    file_name = "{}.json".format(__node_name)

    if os.path.exists(file_name):
        with open(file_name, "r") as log_file:
            out = json.load(log_file)
    else:
        out = {}

    with open(file_name, "w") as log_file:
        out[elect_id] = keys
        json.dump(out, log_file, indent=4)


def adnl_reader(__node_name):
    """
    Читает данные указанной ноды, сохранённые adnl_loger.
    :param __node_name: строка с именем ноды.
    :return: словарь с данными ноды.
    """

    file_name = "{}.json".format(__node_name)
    try:
        with open(file_name, "r") as log_file:
            data = json.load(log_file)
    except FileNotFoundError:
        red_print("Address file for {} does not exist!\n".format(__node_name))
        return False
    except PermissionError:
        red_print("No permissions to read address file {}!\n".format(__node_name))
        return False
    else:
        return data


def stake_return(node):
    """
    Заявка на возвращение ставки.
    :param node: экземпляр класса validator.Node.
    """

    if not node.return_stake():
        red_print("No funds to return!\n")
        return False
    else:
        print("Please wait 30 seconds...")
        sleep(30)
        out2 = node.info()
        if out2["back"] == "0":
            green_print("Stake returned!\n")
        else:
            red_print("...\n")
        return True


def validator_req(__node_name, node):
    """
    Заявка на участие в выборах.
    :param __node_name: строка с именем ноды.
    :param node: экземпляр класса validator.Node.
    """

    info = node.status
    bid = get_bid(info)

    if not bid:
        return
    else:
        out = node.validator(bid)
        if out:
            election_id = info["el_id"]
            adnl_loger(__node_name, out, election_id)
        else:
            red_print("There are currently no validator election!\n")

    return


def is_validator_check(__node_name, node):
    """
    Проверяет стала указаная нода валидатором в текущем периоде.
    :param __node_name: строка с именем ноды.
    :param node: экземпляр класса validator.Node.
    :return: возврашает булеву состояния валидатора или строку "err".
    """

    utime = node.status["c_elect"]
    my_adnl = adnl_reader(__node_name)

    if not my_adnl:
        return "err"

    try:
        my_adnl = my_adnl[utime]["adnlAddr"]

    except KeyError:
        red_print("There is no entry for the current election!")
        return "err"
    else:
        return node.is_validator(my_adnl)


def node_sync(node):
    """
    Проверяет синхронизацию ноды.
    :return: возврашает булеву статуса синхронизации и строку с именем ноды.
    """

    out, diff_time = node.check_sync()
    print("\nTime diff: {} c".format(diff_time))

    return out


def main():
    """
    Основной цикл: выбор режима работы.
    """

    node_name, node_data = node_choice()

    if node_data["net_name"] == 'net.ton.dev':
        validator.tonos("config", "--url", "https://net.ton.dev")
        node = validator.TfNode(node_data, node_name)
    elif node_data["net_name"] == 'main.ton.dev':
        validator.tonos("config", "--url", "https://main.ton.dev")
        node = validator.TfNode(node_data, node_name)
    else:
        node = validator.TONNode(node_data, node_name)

    while True:
        line = '''Mode:\n
        1)Get general info 
        2)Return stake
        3)Send election request
        4)Check node synchronization
        5)Check node validator status\n
        0)Exit
        '''
        print("Used node: {}".format(node_name))
        print(line)
        sys.stdin = open("/dev/tty")
        a = None
        while a not in {'1', '2', '3', '4', '5', '0'}:
            a = input("Choose mode[1-5]: ")

        if a == '1':
            node.info_print()

        elif a == '2':
            if stake_return(node):
                node.update()

        elif a == '3':
            validator_req(node_name, node)
            node.update()

        elif a == '4':
            out = node_sync(node)

            if out:
                green_print("\nNode {} synchronized!\n".format(node_name))
            else:
                red_print("\nNode {} not synchronized!\n".format(node_name))

        elif a == '5':
            out = is_validator_check(node_name, node)

            if out == "err":
                pass
            elif out is False:
                red_print("\nNode {} is not a validator or there is no record of its ADNL address.\n".format(node_name))
            elif out is True:
                green_print("\nNode {} is a validator in the current period!\n".format(node_name))

        elif a == '0':
            exit()


if __name__ == "__main__":
    main()
