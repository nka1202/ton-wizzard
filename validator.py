#! /usr/bin/python3


import subprocess
import json
import base64
from datetime import datetime
from parse import search, findall
from tempfile import mkstemp
from re import sub
from os.path import abspath


def nano_to_gram(ng):
    """
    Принимает строку c количеством нанограмам, возврашает строку с грамами.
    :param ng: строка с нанограмами.
    :return: строка с грамами.
    """
    ng = int(ng)
    gram = ng / 1000000000

    return str(gram)


def gram_to_nano(gr):

    ng = int(gr) * 1000000000

    return str(ng)


def human_time(timestamp):
    """
    Принимает unix-таимстампы.
    Возврашает человекочитаемое время по времени сервера
    (по умолчанию UTC+0/GMT)
    :param timestamp: строка с UNIX-таймстампом.
    :return: строка с человекочитаемым временем.
    """

    return datetime.fromtimestamp(int(timestamp))


def boc_to_base(file):
    """
    Читает файлы и возврашает его содержимое в base64.
    :param file: путь к файлу
    :return: строка в base64
    """
    with open(file, 'rb') as boc:
        base_boc = base64.b64encode(boc.read()).decode()

    return base_boc


def tonos(*cmd):
    """
    Вызов tonos-cli с приенятым параметром.
    :param cmd: команда для интепритатора tonos-cli.
    :return: неформатированная строка с выводом tonos.
    """

    err_line = '''\nTONos-cli не смог выполнить команду: {}.
    Превышен период ожидания. Проверьте доступность сети и корректность команды.\n'''.format(cmd)

    err = open('tonos_err', 'a')

    cmd_list = ["tonos-cli"]
    cmd_list.extend(cmd)

    call = subprocess.Popen(cmd_list, stdout=subprocess.PIPE, stderr=err)
    try:
        out = call.communicate(timeout=60)
        err.close()
        return out[0].decode()
    except subprocess.TimeoutExpired:
        print(err_line)
        call.kill()
        err.close()
        exit()


class TonosGetConfig:

    def __init__(self):
        self.get1 = self.get_1()
        self.get15 = self.get_15()
        self.get17 = self.get_17()
        self.get34 = self.get_34()

    @staticmethod
    def get_config(param):
        out = tonos("getconfig", param)
        out = sub(r"[\t\n\r]", "", out)
        data = search("Config p{}: {{}}\n".format(param), out + "\n")[0]

        return data

    def get_1(self):
        out = self.get_config("1")
        out = str(search('"{}"', out)[0])

        return out

    def get_15(self):
        out = self.get_config("15")
        out = json.loads(out)

        return out

    def get_17(self):
        out = self.get_config("17")
        out = json.loads(out)

        return out

    def get_34(self):
        out = self.get_config("34")
        out = json.loads(out)

        return out

    def adnl_list(self):
        a_list = []

        for i in self.get34["list"]:
            a_list.append(str(i["adnl_addr"]).upper())

        return a_list

    def elector(self):
        return str(self.get1)

    def elect_for(self):
        return str(self.get15["validators_elected_for"])

    def min_stake(self):
        return str(self.get17['min_stake'])

    def elect_time(self):
        return [str(self.get34["utime_since"]), str(self.get34["utime_until"])]


class Node:

    def __init__(self, node, name):

        self.node_name = name
        self.net = node["net_name"]
        self.srv = "{}:{}".format(node["ip"], node["port_srv"])
        self.lite = "{}:{}".format(node["ip"], node["port_con"])
        self.pub = self.key_path(node["pub"])
        self.ls_pub = self.key_path(node["lite_pub"])
        self.client = self.key_path(node["client"])
        self.w_name = self.key_path(node["wal_name"])
        self.w_addr = node["wal_addr"]
        self.max_f = node["max_factor"]

    def key_path(self, file_name):
        return abspath("nodes/{}/keyring/{}".format(self.node_name, file_name))

    def check_sync(self):
        """
        Прверяет синхронизацию выбраной ноды.

        :return: булева со статусом синхронизации ноды и строка с разницей в с.
        """
        time_list = []
        out = self.eng_cons("getstats")
        times = ["unixtime\\t\\t\\t{}\\n", "masterchainblocktime\\t\\t\\t{}\\n"]
        for t_mark in times:
            time_list.append(search(t_mark, out)[0])
        diff_time = int(time_list[0]) - int(time_list[1])

        return diff_time <= 20, diff_time

    def is_validator(self, _adnl):
        """
        Проверяет есть ли данный ADNL адрес в списке валидаторов.
        :param _adnl: ADNL адрес.
        :return: возвращает булеву по наличаю принятого адреса в списке.
        """
        return _adnl in self.adnl_list

    def eng_cons(self, cmd):
        """
        Вызов TON validator-engine-console для указаного сервера с приенятым параметром.
        :param cmd: команда для интепритатора validator-engine-console.
        :return: неформатированая строка с выводом console.
        """

        err_line = '''Engine-console не смог выполнить команду {}, для сервера по адресу {}.
        Превышен период ожидания. Проверьте доступность сети, сервера и корректность команды.'''.format(cmd, self.srv)

        err = open('temp.txt', 'a')

        cmd_string = ["validator-engine-console", "-k", self.client, "-p", self.pub, "-a", self.srv, "-c", cmd]
        call = subprocess.Popen(cmd_string, stdout=subprocess.PIPE, stderr=err)
        try:
            out = call.communicate(timeout=120)
            err.close()
            return str(out)
        except subprocess.TimeoutExpired:
            print(err_line)
            call.kill()
            err.close()
            exit()

    def lite_client(self, cmd):
        """
        Вызов TON lite-client с приенятым параметром.
        :param cmd: команда для интепритатора lite-client.
        :return: неформатированная строка с выводом lc.
        """

        err_line = '''Lite-client не смог выполнить команду: {}.
        Превышен период ожидания. Проверьте доступность сети и корректность команды.'''.format(cmd)

        err = open('lite_err', 'a')

        cmd_string = ["lite-client", "-a", self.lite, "-p", self.ls_pub, "-c", cmd]

        call = subprocess.Popen(cmd_string, stdout=subprocess.PIPE, stderr=err)
        try:
            out = call.communicate(timeout=60)
            err.close()
            return str(out)
        except subprocess.TimeoutExpired:
            print(err_line)
            call.kill()
            err.close()
            exit()

    def compute_return_stake(self, elect_addr):
        """
        :return: строка с суммой к возвраиту в нанограмах.
        """

        cmd = " runmethod -1:{} compute_returned_stake 0x{}".format(elect_addr, self.w_addr)
        out = self.lite_client(cmd)
        out = search("result:  [ {} ]", str(out))[0]

        return out

    def newkey(self):
        """
        Генерирует новый ключ на ноде и возврашает его хэш.
        :return: хэш нового ключа.
        """

        out = self.eng_cons("newkey")
        new_key = search(" new key {}\\n", out)[0]

        return new_key

    def exportpub(self, key):
        """
        Принимает хэш ключа на ноде и возврашает pubkey.
        :param key: хэш ключа.
        :return: открытый ключ.
        """

        cmd = "exportpub " + key
        out = self.eng_cons(cmd)
        pubkey = search("public key: {}\\n", out)[0]

        return pubkey

    def addpermkey(self, key):
        """
        Принимает хэш ключа на ноде и создаёт пермаментный ключ с временем
        жизни от начала выборов до окончания жизни валидатора + 1 секунда
        :param key: хэш ключа.
        """

#        el_time = self.elect_time()[1]
#        el_for = self.validator_end_time()
        cmd_line = "addpermkey {} {} {}".format(key, self.ntime, self.etime)
        self.eng_cons(cmd_line)

    def addtempkey(self, key):
        """
        Принимает хэш ключа и добавляет временный ключ.
        :param key: хэш ключа.
        """

        cmd_line = "addtempkey {} {} {}".format(key, key, self.etime)
        self.eng_cons(cmd_line)

    def add_adnl(self, adnl_key):
        """
        Регистрирует ADNL на ноде.
        :param adnl_key: ключ ADNL.
        """

        cmd = "addadnl {} {}".format(adnl_key, "0")
        self.eng_cons(cmd)

    def add_validator_addr(self, n_key, a_key):
        """
        Принимает ADNL адрес и хэш закрытого ключа и добавляет адрес валидатора.
        :param n_key: хэш закрытого ключа.
        :param a_key: ключ ADNL.
        """

#        el_for = int(self.validator_end_time()) + 1
        cmd = 'addvalidatoraddr {} {} {}'.format(n_key, a_key, self.etime)
        self.eng_cons(cmd)

    def adnl(self):
        """Генерирует новые ключи на указаном сервере.
        :return:
        new_key   <= новый ключ валидатора.
        pub_key   <= публичный ключ валидатора.
        adnl_key  <= ADNL адрес."""

        new_key = self.newkey()
        pub_key = self.exportpub(new_key)
        self.addpermkey(new_key)
        self.addtempkey(new_key)

        adnl_key = self.newkey()
        self.add_adnl(adnl_key)

        return new_key, pub_key, adnl_key

    def validator_req(self, adnl_addr):
        """
        Генерирует запрос на участие в выборах.
        :param adnl_addr: Получает адрес ADNL.
        :return: хэш подписи.
        """

        err = open('temp.txt', 'a')

        cmd_string = ["fift", "-s", "validator-elect-req.fif", "-1:{}".format(self.w_addr), self.ntime, self.max_f, adnl_addr]
        call = subprocess.Popen(cmd_string, stdout=subprocess.PIPE, stderr=err)
        out = str(call.communicate())
        sign = search("\\n{}\\n", out)[0]
        err.close()

        return sign

    def sign_req(self, n_key, sign):
        """
        Подписывает запрос на валидацию.
        :param n_key: Новый ключ ноды.
        :param sign: хэш подписи от validator_req
        :return: подпись запроса.
        """
        cmd = "sign {} {}".format(n_key, sign)
        out = self.eng_cons(cmd)
        sign_out = search("got signature {}\\n", out)[0]
        print("\nSignature: {}".format(sign_out))

        return sign_out

    def elect_sign(self, a_key, pub_key, sign, tmp_file):
        """
        :param a_key: ADNL ключ.
        :param pub_key: открытый ключ ноды.
        :param sign: подпись запроса созданый sign_req
        :param tmp_file:
        """

#        utime = self.elect_time()[1]

        err = open('temp.txt', 'a')
        cmd_string = ["fift", "-s", "validator-elect-signed.fif", "-1:{}".format(self.w_addr), self.ntime, self.max_f,
                      a_key, pub_key, sign, tmp_file]
        call = subprocess.Popen(cmd_string, stdout=subprocess.PIPE, stderr=err)
        call.communicate()
        err.close()

    def info_print(self):
        """
        Вывод данных полученных info()
        """
        print("Info:\n")
        print('Network name: {}'.format(self.net))
        print('Current election: {}'.format(human_time(self.status['c_elect'])))
        print('Next election: {}'.format(human_time(self.status['n_elect'])))
        print('Balance: {}'.format(nano_to_gram(self.status['balance'])))
        print('seqno: {}'.format(self.status['seqno']))
        print('Elector address: -:1{}'.format(self.status['ea']))
        print('Election id: {}'.format(self.status['el_id']))
        print('Validator expiration: {}'.format(human_time(self.status['end_val'])))
        print('Minimum bid: {}'.format(nano_to_gram(self.status['min_bid'])))
        print('Ammount to return: {}\n'.format(nano_to_gram(self.status['back'])))


class TONNode(Node):

    """
    Библиотека функций для врарпера TON lite-client и  validator-engine-console.
    """

    def __init__(self, node, name):
        """
        Экземпляр класса проинимает словарь со свойствами ноды.
        :param node:
        {"ip" : "ip адрес",
        "port_srv" : "контрольный порт ноды",
        "port_con" : "порт lite-server",
        "pub" : "публичный_ключ.pub",
        "wal_addr" : "адрес кошелька в BASE64",
        "wal_name" : "имя кошелька",
        "net_name" : "имя сети",
        "max_factor" : "максимальный коэфицент возврата",
        "comment" : "комментанрий"}
        """

        super().__init__(node, name)

        self.el_addr = self.elect_addr()
        self.status = self.info()
        self.adnl_list = self.get_validators_adnl()
        self.utime = self.status["c_elect"]
        self.ntime = self.status["n_elect"]
        self.etime = self.validator_end_time()

        self.at_name = "@{}.addr".format(node["wal_name"])
        self.max_f = node["max_factor"]

    def update(self):
        self.el_addr = self.elect_addr()
        self.status = self.info()
        self.adnl_list = self.get_validators_adnl()
        self.utime = self.status["c_elect"]
        self.ntime = self.status["n_elect"]
        self.etime = self.validator_end_time()

    def get_seqno(self):
        """
        Возврашает seqno для указаного кошелька.
        :return:  seqno
        """

        cmd = " runmethod -1:{} seqno".format(self.w_addr)
        out = self.lite_client(cmd)
        seqno = search("result:  [ {} ]", out)[0]

        return seqno

    def get_validators_adnl(self):
        """
        Получает данные текущих валидаторов.
        :return: список ADNL адресов валидаторов.
        """

        cmd = " getconfig 34"
        out = self.lite_client(cmd)
        adnls = [addr[0] for addr in findall("adnl_addr:x{})", out)]

        return adnls

    def elect_addr(self):
        """
        Возврашает адрес текушего избирателя.
        :return: адрес текушего избирателя
        """

        out = self.lite_client(" getconfig 1")
        addr = search("elector_addr:x{})", out)[0]

        return addr

    def min_stake(self):
        """
        Возврашает минимальный размер ставки
        :return: минимальный размер ставки в нанограмах
        """

        out = self.lite_client(" getconfig 17")
        stake = search("len:6 value:{}))", out)[0]  # возможна смена формата строки

        return stake

    def election_id(self):
        """
        Узнаёт id выборов.
        :return: id текущих выборов.
        """

        cmd = " runmethod -1:{} active_election_id".format(self.el_addr)
        out = self.lite_client(cmd)
        ell_id = search("result:  [ {} ]", out)[0]

        return ell_id

    def balance(self):
        """
        Узнаёт баланс кошелька.
        :return: строка с балансом кошелька в нанограмах.
        """

        cmd = " getaccount -1:{}".format(self.w_addr)
        out = self.lite_client(cmd)
        bal = search("account balance is {}ng", out)[0]

        return bal

    def elect_time(self):
        """
        Узнаёт таимстампы выборов.
        :return: список с unix таимстаспами выборов:
        [0]  <= таимстамп текушего периода.
        [1]  <= таимстамп следуюшего периода."""
        time_list = []
        out = self.lite_client(" getconfig 34")
        times = ["utime_since:{} ", "utime_until:{} "]
        for t_mark in times:
            time_list.append(search(t_mark, out)[0])

        return time_list

    def validators_elected_for(self):
        """
        Узнаёт время на которое выбераются валидаторы.
        :return: строка с временем на которое выбераются валидаторы в секундах.
        """

        out = self.lite_client(" getconfig 15")
        el_for = search("( validators_elected_for:{} ", out)[0]

        return el_for

    def validator_end_time(self):
        """
        Возврашает таимстамп окончания жизни валидатора.
        :return: строка с таимстампом
        """

#        el_time = self.elect_time()[1]
        el_for = int(self.elect_time()[1]) + int(self.validators_elected_for()) + 1

        return str(el_for)

    def send_file(self, file_name):
        """
        Принимает имя файла и отправляет его lite-client`ом.
        :param file_name: Имя файла.
        """

        cmd = 'sendfile {}'.format(file_name)
        self.lite_client(cmd)

    def my_wallet(self, bid, temp_file1, temp_file2):
        """
        Генерирует wallet-query.boc.
        """
        seqno = self.get_seqno()
#        dest_addr = self.elect_addr()

        err = open('err_log', 'a')
        cmd_string = ["fift", "-s", "wallet.fif", self.w_name, "-1:{}".format(self.status['ea']), seqno, bid, "-B",
                      temp_file1, temp_file2]
        call = subprocess.Popen(cmd_string, stdout=subprocess.PIPE, stderr=err)
        out = call.communicate()
        err.close()

        return out

    def gen_return_query(self, amnt, temp_file):
        """
        :param amnt: сумма возврата(пока '1.')
        :param temp_file: name of temporaly file
        :return: генерирует wallet-query.boc
        """
        seqno = self.get_seqno()

        err = open('err_log', 'a')
        cmd_string = ["fift", "-s", "recover-stake.fif", temp_file]
        call1 = subprocess.Popen(cmd_string, stdout=subprocess.PIPE, stderr=err)
        call1.communicate()

        cmd_string = ["fift", "-s", "wallet.fif", self.w_name, "-1:{}".format(self.status['ea']), seqno, amnt, "-B",
                      temp_file]
        call2 = subprocess.Popen(cmd_string, stdout=subprocess.PIPE, stderr=err)
        call2.communicate()

        err.close()

    def info(self):
        """Сбор и вывод информации о текущем статусе.
        :return: словарь out со следующими ключами:
        c_elect  <= таймстамп текущих выборов(elect_time[0]).
        n_elect  <= таймстамп следующих выборов(elect_time[1]).
        balance  <= текущий баланс кошелька.
        ea       <= адрес выборшика.
        seqno    <= текущий seqno.
        el_id    <= id следующих выборов(по идее el_id == n_elect).
        end_val  <= таймстамп окончания срока валидатора.
        back     <= сума к возврату в нанограмах.
        min_bid  <= минимальный размер ставки
        """

        out = {}
        out['c_elect'] = self.elect_time()[0]
        out['n_elect'] = self.elect_time()[1]
        out['balance'] = self.balance()
        out['ea'] = self.elect_addr()
        out['seqno'] = self.get_seqno()
        out['el_id'] = self.election_id()
        out['end_val'] = self.validator_end_time()
        out['back'] = self.compute_return_stake(out['ea'])
        out['min_bid'] = self.min_stake()

        return out

    def return_stake(self):
        """
        Возврашает ставку.
        :return если есть средства к возврату делает попытку и возврашает True, иначе False.
        """

        if self.status['back'] == '0':
            return False
        else:
            tmp = mkstemp()[1]
            self.gen_return_query("1.", tmp)
            self.send_file(tmp)
            return True

    def validator(self, bid):
        """
        Гененирует ключи и регистрирует валидатор.
        :param bid: размер ставки в грамах.
        :return: возврашает словарь с ключами:
        newKey   <= новый ключ валидатора.
        pubKey   <= публичный ключ валидатора.
        adnlAddr  <= ADNL адрес.
        electionBid <= размер ставки.
        или False если нет текущих выборов.
        """
        el = self.status["el_id"]
        if el == "0":
            return False
        else:
            n_key, p_key, a_key = self.adnl()
            print("\nНовый ключ: {}".format(n_key))
            print("Новый pubkey: {}".format(p_key))
            print("Новый ADNL: {}\n".format(a_key))

            tmp = mkstemp()[1]
            sign = self.validator_req(a_key)
            sign_out = self.sign_req(n_key, sign)
            self.elect_sign(a_key, p_key, sign_out, tmp)

            tmp2 = mkstemp()[1]
            self.my_wallet(bid, tmp, tmp2)
            self.send_file(tmp2)

            out = {}
            out['newKey'] = n_key
            out['privatKey'] = p_key
            out['adnlAddr'] = a_key
            out["electionBid"] = bid

            return out


class TfNode(Node):

    def __init__(self, node, name):
        """
        Экземпляр класса проинимает словарь со свойствами ноды.
        :param node:
        {"ip" : "ip адрес",
        "port_srv" : "контрольный порт ноды",
        "port_con" : "порт lite-server",
        "pub" : "публичный_ключ.pub",
        "wal_addr" : "адрес кошелька в BASE64",
        "wal_name" : "имя кошелька",
        "net_name" : "имя сети",
        "max_factor" : "максимальный коэфицент возврата",
        "comment" : "комментанрий"}
        """

        super().__init__(node, name)

        self.abi = abspath("tonos/SafeMultisigWallet.abi.json")
        self.at_name = "-1:{}".format(self.w_addr)

        self.status = self.info()

        self.el_addr = self.status["ea"]
        self.utime = self.status["c_elect"]
        self.ntime = self.status["n_elect"]
        self.adnl_list = self.status['a_list']
        self.elect_for = self.status['elect_for']

        self.utime = self.status["c_elect"]
        self.ntime = self.status["n_elect"]
        self.etime = self.validator_end_time()
        self.status['end_val'] = self.etime

    def update(self):
        self.status = self.info()
        self.status['end_val'] = self.validator_end_time()
        self.el_addr = self.status["ea"]
        self.adnl_list = self.status['a_list']
        self.utime = self.status["c_elect"]
        self.ntime = self.status["n_elect"]
        self.elect_for = self.status['elect_for']
        self.etime = self.validator_end_time()

    def balance(self):
        """
        Узнаёт баланс кошелька.
        :return: строка с балансом кошелька в нанограмах.
        """

        out = tonos("account", "-1:{}".format(self.w_addr))
        bal = search("balance:       {}\n", out)[0]

        return bal

    def election_id(self):
        """
        Узнаёт id выборов.
        :return: id текущих выборов.
        """

        out = tonos("runget", "-1:{}".format(self.el_addr), "active_election_id")
        elect_id = search('Result: ["{}"]', out)[0]
        elect_id = int(elect_id, 16)

        return str(elect_id)

    def validator_end_time(self):
        """
        Возврашает таимстамп окончания жизни валидатора.
        :return: строка с таимстампом
        """

        el_for = int(self.ntime) + int(self.elect_for) + 1

        return str(el_for)

    def tonos_param(self, boc_file, value):
        params = {}
        params["dest"] = "-1:{}".format(self.el_addr)
        params["value"] = value
        params["bounce"] = True
        params["allBalance"] = False
        params["payload"] = boc_to_base(boc_file)

        return json.dumps(params, separators=(',', ':'))

    def tonos_send(self, boc_name, value="1000000000"):

        params = self.tonos_param(boc_name, value)
        out = tonos("call", "-1:{}".format(self.w_addr), "submitTransaction", params, "--sign", self.w_name, "--abi", self.abi)
        print(out)

    def info(self):
        """Сбор и вывод информации о текущем статусе.
        :return: словарь out со следующими ключами:
        c_elect  <= таймстамп текущих выборов(elect_time[0]).
        n_elect  <= таймстамп следующих выборов(elect_time[1]).
        balance  <= текущий баланс кошелька.
        ea       <= адрес выборшика.
        seqno    <= текущий seqno.
        el_id    <= id следующих выборов(по идее el_id == n_elect).
        end_val  <= таймстамп окончания срока валидатора.
        back     <= сума к возврату в нанограмах.
        min_bid  <= минимальный размер ставки
        """

        net_cfg = TonosGetConfig()
        self.el_addr = net_cfg.get1
        out = {}
        out['c_elect'] = net_cfg.elect_time()[0]
        out['n_elect'] = net_cfg.elect_time()[1]
        out['balance'] = self.balance()
        out['ea'] = net_cfg.get1
        out['seqno'] = "N/A"
        out['el_id'] = self.election_id()
        out['back'] = self.compute_return_stake(out['ea'])
        out['min_bid'] = net_cfg.min_stake()
        out['a_list'] = net_cfg.adnl_list()
        out['elect_for'] = net_cfg.elect_for()

        return out

    def return_stake(self):
        """
        Возврашает ставку.
        :return если есть средства к возврату делает попытку и возврашает True, иначе False.
        """

        if self.status['back'] == '0':
            return False

        else:
            tmp = mkstemp()[1]

            err = open('err_log', 'a')
            cmd_string = ["fift", "-s", "recover-stake.fif", tmp]
            call = subprocess.Popen(cmd_string, stdout=subprocess.PIPE, stderr=err)
            call.communicate()
            self.tonos_send(tmp)

            err.close()
            return True

    def validator(self, bid):
        """
               Гененирует ключи и регистрирует валидатор.
               :param bid: размер ставки в грамах.
               :return: возврашает словарь с ключами:
               newKey   <= новый ключ валидатора.
               pubKey   <= публичный ключ валидатора.
               adnlAddr  <= ADNL адрес.
               electionBid <= размер ставки.
               или False если нет текущих выборов.
               """
        el = self.status["el_id"]
        if el == "0":
            return False
        else:
            n_key, p_key, a_key = self.adnl()
            print("\nНовый ключ: {}".format(n_key))
            print("Новый pubkey: {}".format(p_key))
            print("Новый ADNL: {}\n".format(a_key))

            tmp = mkstemp()[1]
            sign = self.validator_req(a_key)
            sign_out = self.sign_req(n_key, sign)
            self.elect_sign(a_key, p_key, sign_out, tmp)
            self.tonos_send(tmp, gram_to_nano(bid))

            out = {}
            out['newKey'] = n_key
            out['privatKey'] = p_key
            out['adnlAddr'] = a_key
            out["electionBid"] = bid

            return out
