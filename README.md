# TON-Wizzard

Simple script that control remote TON nodes.

## Features

 - Simple cli wizzard.
 - Stores keys and run on local computer.
 - Work with multiple nodes.
 - Work with TONLabs and TON-test nodes.
 - Can use both tonos-cli and lite-client.
 - Set tonos-cli config for net.ton.dev or main. ton.dev.
 - Installation script automatically compiles dependencies(tonos-cli, fift, lite-client etc.) 

## Requirements

 - Ubuntu 18.04.4
 - Python 3.6+

## Installation

I recomend to use clean encrypted virtual machine with ubuntu 18.04.4, 2 cpu, 2Gb ram and 10gb free disk space.

1) git clone https://gitlab.com/nka1202/ton-wizzard.git && cd ton-wizzard

2) bash install.sh

3) mkdir -p nodes/{your_node_name}/keyring

## Configure
If You use node instaled from github.com/tonlabs/net.ton.dev:

1) Rename servers_example.json to servers.json 

2) Replace "ip": "127.0.0.1" to your node ip address and wal_addr: "351...d36" to your wallet hex address.

3) Copy files from ton-keys directory on your node to ~/ton-wizzard/nodes/node/keyring/

Else:

1) you must create servers.json file in ton-wizzard dir(see description and servers_example.json in work dir)

2) create folder {script_dir}/nodes/{node_name}/keyring

3) copy lite-client public key, validator-engine-console keys and wallet keyfile to keyring dir.

## servers.json description

    "{your_node_name}": {                                       # same as keyring path 
        "ip": "{node ip address}",                              # ipv4 address
        "port_srv": "{validator_engine_console_port}",          # "3030" by default
        "port_con": "{lite-client_port}",                       # "3031" by default
        "pub": "{node_public_key}",                             # server.pub
        "lite_pub" : "{Lite-client_public_key}",                # "liteserver.pub"
        "client" :  "{validator-console_private_key}",          # "client"
        "wal_addr" : "{hex_wallet_address}",                    # "3515a3728777597f1be4abc9d795096cf6be98014b1df84d350b8380e7925d33",
        "wal_name" : "{wallet_file_name}",                      # "msig.keys.json"
        "max_factor" : "{max_factor}",                          # "2.7"
        "net_name" : "{network_name}",                          # "net.ton.dev"
        "comment" : "{TONlab node}"                             # some comment about node
    

{your_node_name}                 <= node name, same as nodes/{your_node_name}/keyring.

{ip}                             <= node IPv4 address.

{validator_engine_console_port}  <= validator-engine-consile port. You can configure it in yor node config (/var/ton-work/db/config.json =>  "control")

{lite-client_port}               <= lite client port. You can configure it in yor node config (/var/ton-work/db/config.json =>  "liteservers")

{server_public_key}              <= node public key. On TONLabs node "server.pub", stored in $HOME/ton-keys"

{Lite-client_public_key}         <= lite-client public key. On TONLabs node "liteserver.pub", stored in $HOME/ton-keys"

{validator-console_private_key}  <= validator-engine-consiole private key. On TONLabs node "liteserver.pub", stored in $HOME/ton-keys"

{hex_wallet_address}             <= wallet hex address without blockchain id("-1:"")

{wallet_file_name}               <= wallet key file name. On TONLabs node "msig.keys.json", stored in $HOME/ton-keys" or wallet_name.addr on telegram nodes

{network_name}                   <= network name. "net.ton.dev" - FreeTON testnet(tonos-cli), "main.ton.dev(tonos-cli)" - FreeTON mainnet, "TON-test" - telegram testnet(lite-client)


## Run
cd ~/ton-wizzard

python3 wizzard.py