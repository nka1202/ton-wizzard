#!/bin/bash -ex

# проверка на запуск от имени root
if [[ $(id -u) -ne 0 ]]
  then echo "Please run this script with sudo!"
  exit 1
fi

script_dir=$(pwd)


mkdir "$script_dir"/tonos
mkdir "$script_dir"/nodes

# устанавливаем переменные окружения для сборки
export UCF_FORCE_CONFFOLD=YES
export DEBCONF_FRONTEND=noninteractive

# устанавливаем необходимые пакеты
sudo apt-get -y purge console-setup
sudo apt-get update
sudo apt-get autoremove -yq
sudo apt-get -o Dpkg::Options::="--force-confold" -o Dpkg::Options::="--force-confdef" install -yq libghc-zlib-dev  pkg-config gperf build-essential make wget ntp libreadline-dev git ccache libmicrohttpd-dev libssl-dev cmake libgflags-dev mc ufw htop python3-pip fail2ban cargo --allow-downgrades --allow-remove-essential  --allow-change-held-packages
sudo apt-get -o Dpkg::Options::="--force-confold" -o Dpkg::Options::="--force-confdef" dist-upgrade -yq

sudo pip3 install parse

# клонируем свежие исходники c++ ноды  из репозитория
tmpdir=$(mktemp -d /tmp/ton.XXXX)
cd "$tmpdir" || exit
git clone --recurse-submodules -j8 https://github.com/ton-blockchain/ton

# создаём директорию для сборки
mkdir "$tmpdir"/ton-build
cd "$tmpdir"/ton-build || exit
cmake "$tmpdir"/ton


# сборка клиента и перемещение готового бинарника в /usr/bin
cmake --build . --target lite-client
sudo cp "$tmpdir"/ton-build/lite-client/lite-client /usr/bin/lite-client
sudo chmod 0755 /usr/bin/lite-client

# сборка консоли и перемещение готового бинарника в /usr/bin
cmake --build . --target validator-engine-console
sudo cp "$tmpdir"/ton-build/validator-engine-console/validator-engine-console /usr/bin/validator-engine-console
sudo chmod 0755 /usr/bin/validator-engine-console

# сборка интерпретатора Fift и перемещение готового бинарника в /usr/bin
cmake --build . --target fift
sudo cp "$tmpdir"/ton-build/crypto/fift /usr/bin/fift
sudo chmod 0755 /usr/bin/fift

# обновляем fift-скрипты
sudo mkdir /usr/lib/fift/
sudo cp "$tmpdir"/ton/crypto/fift/lib/* /usr/lib/fift/
sudo cp -r "$tmpdir"/ton/crypto/smartcont/* /usr/lib/fift/
sudo chmod 0644 /usr/lib/fift

# создаём переменную среды с путём fift-скриптов
echo 'FIFTPATH="/usr/lib/fift"' >> ~/.profile

rm -rf "$tmpdir"

tmpdir=$(mktemp -d /tmp/ton.XXXX)
cd "$tmpdir" || exit
git clone https://github.com/tonlabs/tonos-cli.git
cd "$tmpdir"/tonos-cli/ || exit

cargo update
cargo build --release

cp "$tmpdir"/tonos-cli/target/release/tonos-cli "$script_dir"/tonos/tonos-cli
sudo chmod 0755 "$script_dir"/tonos/tonos-cli

echo "PATH=\"${script_dir}/tonos:$PATH\"" >> ~/.profile

rm -rf "$tmpdir"

cd "$script_dir"/tonos
wget https://raw.githubusercontent.com/tonlabs/ton-labs-contracts/master/solidity/safemultisig/SafeMultisigWallet.abi.json